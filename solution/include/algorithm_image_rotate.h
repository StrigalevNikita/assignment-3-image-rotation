#ifndef ALGORITHM_IMAGE_ROTATE
#define ALGORITHM_IMAGE_ROTATE

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );

struct image new_image(uint64_t width, uint64_t height);

#endif
