#ifndef TYPE_IMAGE
#define TYPE_IMAGE

#include <inttypes.h>


struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

#endif
