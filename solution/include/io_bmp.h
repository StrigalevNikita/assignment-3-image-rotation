#ifndef IO_BMP
#define IO_BMP
#include "type_bmp.h"
#include "type_image.h"
#include <stdio.h>


enum write_status  {
  WRITE_OK = 200,
  WRITE_ERROR = 500
};


enum read_status  {
	READ_OK = 200,
	READ_INVALID_SIGNATURE = 415,
	READ_INVALID_BITS = 400,
	READ_INVALID_HEADER = 412,
	READ_NULL_FILE = 404
};


enum read_status from_bmp( FILE* in, struct image* img );
enum write_status to_bmp( FILE* out, struct image const* img );

enum read_status header_read(struct bmp_header* h, uint64_t status);
struct bmp_header header_write(const uint64_t width, const uint64_t height);
enum read_status image_read(FILE* in, struct image* img, uint64_t status);
enum read_status header_validate(const struct bmp_header head);

#endif
