#ifndef ALGORITHM_UTILITY_MAIN
#define ALGORITHM_UTILITY_MAIN
#include "io_bmp.h"
#include <stdio.h>

/* output */
void print_exception(const char* e);
void print_string(const char* e);
void print_read_status(enum read_status s);
void print_write_status(enum write_status s);

/* main */
void rotation_algorithm(const char* in, const char* out);
uint8_t padding_get(uint64_t width);

#endif
