#include "type_image.h"
#include "algorithm_utility_main.h"
#include <stdio.h>
#include <stdlib.h>



struct image new_image(uint64_t width, uint64_t height) {
    struct image answer = (struct image) {.data = NULL, .width = width, .height = height};

    answer.data = malloc(answer.height * answer.width * sizeof(struct pixel));
    if (answer.data == NULL) {
        print_exception("Allocation error");
        exit(1);
    }
    return answer;
}

struct image rotate( struct image const source ){
	struct image answer = new_image(source.height, source.width);
	if (answer.data == NULL){
		return answer;
	}
	else{
		for (uint64_t y = 0; y < source.height; y++) {
        		for (uint64_t x = 0; x < source.width; x++) {
            			struct pixel old = source.data[y * source.width + x];
            			size_t newX = source.height - (y + 1);
            			size_t newY = x;
            			answer.data[answer.width * newY + newX] = old;
        		}
		}
		return answer;
	}
}
