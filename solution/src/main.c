#include "algorithm_utility_main.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
	 	print_exception("Wrong arguments count!\nExample: ./image-transformer <source-image> <transformed-image>");
		return 1;
	}
	rotation_algorithm(argv[1], argv[2]);
	return 0;
}
