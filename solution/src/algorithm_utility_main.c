#include "algorithm_utility_main.h"
#include "algorithm_image_rotate.h"
#include "io_bmp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint8_t padding_get(uint64_t width) {
    uint64_t total = width * sizeof(struct pixel);
    return (4 - total % 4) % 4;
}

void print_exception(const char* e){
	fprintf(stderr, "%s \n", e);
}

void print_string(const char* e){
	fprintf(stdout, "%s \n", e);
}

void print_read_status(enum read_status s){
	switch (s){
		case READ_INVALID_SIGNATURE:
			print_exception("READ_INVALID_SIGNATURE exception");
			break;
		case READ_NULL_FILE:
			print_exception("READ_NULL_FILE exception");
			break;
		case READ_INVALID_BITS:
			print_exception("READ_INVALID_BITS exception");
			break;
		case READ_INVALID_HEADER:
			print_exception("READ_HEADER exception");
			break;
		case READ_OK:
			print_string("Read");
			break;
			
	}
}

void print_write_status(enum write_status s){
	switch (s){
		case WRITE_ERROR:
			print_exception("WRITE_ERROR exception");
			break;
		case WRITE_OK:
			print_string("Wrote");
			break;
			
	}
}

void rotation_algorithm(const char* in, const char* out){
	FILE* f = fopen(in, "rb");

	struct image* im = malloc(sizeof(struct image));
	enum read_status status = from_bmp(f, im);
	fclose(f);
	switch (status){
		case READ_INVALID_BITS:
			print_exception("Invalid bits");
			break;
		case READ_INVALID_HEADER:
			print_exception("Header is invalid");
			break;
		case READ_INVALID_SIGNATURE:
			print_exception("Signature is invalid");
			break;
		case READ_NULL_FILE:
			print_exception("File is null");
			break;
		case READ_OK:
			f = fopen(out, "wb");
			struct image* answer = malloc(sizeof(struct image));
			*answer = rotate(*im);
			enum write_status status = to_bmp(f, answer);
			if (!(status == WRITE_OK)) {
				print_exception("Write error");
			}
			//free
			free(im -> data);
			free(im);
			fclose(f);
			free(answer -> data);
			free(answer);
			break;
	}
}
