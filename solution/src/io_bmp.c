#include "io_bmp.h"
#include "algorithm_image_rotate.h"
#include "algorithm_utility_main.h"
#include "type_bmp.h"
#include "type_image.h"

#define BMP_CONST_TYPE 0x4D42
#define BMP_CONST_RESERVED 0
#define BMP_CONST_SIZE 40
#define BMP_CONST_PLANES 1
#define BMP_CONST_BIT_COUNT 24
#define BMP_CONST_COMPRESSION 0
#define BMP_CONST_X 0
#define BMP_CONST_Y 0
#define BMP_CONST_CLR_USED 0
#define BMP_CONST_IMAGE_SIZE 0
#define BMP_CONST_CLR_IMPORTANT 0
#define BMP_CONST_TYPE_MEMORY sizeof(struct bmp_header)

struct bmp_header header_write(uint64_t width, uint64_t height) {
    uint8_t padding = padding_get(width);
    struct bmp_header header;
    header.bfType = BMP_CONST_TYPE;
    header.bfileSize = height * (width * sizeof(struct pixel) + padding) + sizeof(struct bmp_header);
    header.bfReserved = BMP_CONST_RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_CONST_SIZE;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = BMP_CONST_PLANES;
    header.biBitCount = BMP_CONST_BIT_COUNT;
    header.biCompression = BMP_CONST_COMPRESSION;
    header.biSizeImage = BMP_CONST_IMAGE_SIZE;
    header.biXPelsPerMeter = BMP_CONST_X;
    header.biYPelsPerMeter = BMP_CONST_Y;
    header.biClrUsed = BMP_CONST_CLR_USED;
    header.biClrImportant = BMP_CONST_CLR_IMPORTANT;
    return header;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
	struct bmp_header h;
	if (!fread(&h, BMP_CONST_TYPE_MEMORY, 1, in)) {
		return READ_INVALID_HEADER;
	}
	if (h.biBitCount != BMP_CONST_BIT_COUNT){
		return READ_INVALID_BITS;
	}
	if (h.bfType != BMP_CONST_TYPE){
		return READ_INVALID_SIGNATURE;
	}
	*img = new_image(h.biWidth, h.biHeight);
	if (img -> data == NULL) {
		return READ_NULL_FILE;
	}
	for (uint64_t y = 0; y < img -> height; y++) {
		if(img -> width != fread((img -> width * y + img -> data), sizeof(struct pixel), img -> width, in)){
            return READ_INVALID_BITS;
        }
        if(fseek(in, padding_get(img -> width), SEEK_CUR)) {
        	return READ_INVALID_BITS;
        } 
	}
    	if(!img){ 
		return READ_NULL_FILE;
	}
	print_string("Read");
    	return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    uint8_t padding = padding_get(img -> width);
    struct bmp_header h = header_write(img -> width, img -> height);
    if (fwrite(&h, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    for (uint64_t y = 0; y < img -> height; y++) {
        if(fwrite(y * img -> width + img -> data, sizeof(struct pixel) * img -> width, 1, out) != 1) {
            return WRITE_ERROR;
        }
        uint64_t temp = 0;
        if(padding != 0) {
            if(fwrite(&temp, padding, 1, out) != 1){
		return WRITE_ERROR;
	}
        }
    }
    print_string("Wrote");
    return WRITE_OK;
}
